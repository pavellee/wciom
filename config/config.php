<?php
return [
    'id' => 'wciom-test',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'wciom\controllers',
    // установим псевдоним '@micro', чтобы включить автозагрузку классов из пространства имен 'micro'
    'aliases' => [
        '@wciom' => __DIR__ . '/../src',
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=wciom',
            'username' => 'root',
            'password' => '1',
            'charset' => 'utf8',
        ],
    ],
    'controllerMap' => [
        'load' => 'wciom\commands\LoadCommand',
        'get' => 'wciom\commands\GetCommand'
    ]

];