<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%phones}}`.
 */
class m210601_133210_create_phone_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%phone}}', [
            'id' => $this->primaryKey(),
            'number' => $this->char(11)->notNull(),
            'gender' => $this->char(1)->notNull()->defaultValue(''),
            'age' => $this->tinyInteger()->notNull()->defaultValue(0),
            'region_id' => $this->integer()->notNull()->defaultValue(0)
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('phone_region_id_region_id_fk', '{{%phone}}');
        $this->dropTable('{{%phone}}');
    }
}
