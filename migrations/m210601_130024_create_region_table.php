<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%region}}`.
 */
class m210601_130024_create_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%region}}', [
            'region_id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull()->unique()
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%region}}');
    }
}
