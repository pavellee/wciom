<?php


namespace wciom\models;


use Yii;
use yii\db\ActiveRecord;

class Region extends ActiveRecord
{
    public static function tableName()
    {
        return '{{region}}';
    }

    public function rules()
    {
        return [
            ['region', 'string', 'max' => 50]
        ];
    }


    public static function batchInsert(array $regions)
    {
        $db = Yii::$app->db;

        $command = $db->createCommand();

        $data_for_insert = array_map(function ($region) use ($db) {
            return '(' . $db->quoteValue($region) . ')';
        }, $regions);

        $command->setSql('INSERT IGNORE INTO ' . self::tableName() . ' (`name`) VALUES ' . implode(', ', $data_for_insert))
            ->execute();
    }

    public static function availableIds()
    {
        return Yii::$app->db->createCommand('SELECT region_id FROM region')->queryColumn();
    }

}