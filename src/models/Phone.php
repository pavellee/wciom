<?php

namespace wciom\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property string number
 * @property string age
 * @property string gender
 * @property string region_id
 * @property string id
 */
class Phone extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{phone}}';
    }

    public static function batchInsert(array $data)
    {
        Yii::$app->db->createCommand()->batchInsert(self::tableName(), ['number', 'age', 'gender', 'region_id'], $data)->execute();
    }

    public function rules(): array
    {
        return [
            ['number', 'match', 'pattern' => '/^\d{11}$/i'],
            ['gender', 'in', 'range' => ['m', 'f']],
            ['age', 'number', 'max' => 127, 'min' => 1],
            ['number', 'required'],
            ['region_id', 'number']
        ];
    }

    public function number(): string
    {
        if ($this->validate('number')) {
            return $this->number;
        }

        return "";
    }

    public function age(): int
    {
        if ($this->validate('age')) {
            return (int)$this->age;
        }

        return 0;
    }

    public function gender(): string
    {
        if ($this->validate('gender')) {
            return $this->gender;
        }

        return "";
    }

    public function region(): ?string
    {
        if ($this->validate('region_id')) {
            $region = Region::findOne($this->region_id);

            return !is_null($region) ? $region->name : '';
        }

        return "";
    }

    public function id(): int
    {
        return (int)$this->id;
    }

}