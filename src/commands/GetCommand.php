<?php


namespace wciom\commands;


use wciom\models\Phone;
use yii\console\Controller;

class GetCommand extends Controller
{
    public function actionPhones()
    {
        $phones = Phone::find()->all();

        /** @var Phone $phone */
        foreach ($phones as $phone) {
            echo "id - {$phone->id()}, телефон - {$phone->number()}, возраст - {$phone->age()}, пол - {$phone->gender()}, регион - {$phone->region()}\n";
        }
    }

    public function actionPhoneById($id)
    {
        $phone = Phone::findOne($id);

        echo (!is_null($phone) ?
                "id - {$phone->id()}, телефон - {$phone->number()}, возраст - {$phone->age()}, пол - {$phone->gender()}, регион - {$phone->region()}" :
                "Запись не найдена") . "\n";
    }
}