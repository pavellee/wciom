# ВЦИОМ
## тестовое задание

### запуск приложения

```bash
chmod +x console.sh
./console.sh
```
Произойдет первоначальная сборка контейнера и запуск консоли

### БД
```bash
chmod +x yii
./yii migrate
```
Создастся структура БД
###тестовые данные
Загрузка регионов
```bash
./yii load/load-region
```
Добавление тестовых данных для телефонов
```bash
./yii load/add-test-phones
```
Добавление телефона
```bash
./yii load/add-phone {number} {region_id} {gender} {age}
```
###Чтение номеров
Показ номеров
```bash
./yii get/phones
```
Показ номера по id
```bash
./yii get/phone-by-id {id}
```